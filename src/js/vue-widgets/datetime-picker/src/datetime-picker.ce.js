<<<<<<< HEAD
import { defineCustomElement, h } from "vue";
=======
import { defineCustomElement, h, onMounted, watch, reactive, ref } from "vue";
>>>>>>> upstream/master
import App from "./App.vue";
import styles from "./custom.scss?inline";

customElements.define(
    "datetime-picker",
<<<<<<< HEAD
    defineCustomElement({
        render: (props, { slots }) => {
            return h(App, props, slots);
        },
        styles: [styles],
    })
);

new MutationObserver((mutations) => {
    if (mutations.some((m) => m.target.querySelector("datetime-picker"))) {
        const datetimePickers = document.querySelectorAll("datetime-picker");

        /*
            Sync the hidden inputs and the timezone picker with the form element. Essential for data submission.
        */
        datetimePickers.forEach((datetimePicker) => {
            const form = datetimePicker.closest("form");
            form.onsubmit = (e) => {
                e.preventDefault();
                const elements = form.querySelectorAll("datetime-picker");
                elements.forEach((el) => {
                    el.shadowRoot.querySelectorAll("input[type='hidden']").forEach((input) => {
                        form.appendChild(input);
                    });
                    const timezone = el.shadowRoot.querySelector("select[id='timezone']");
                    if (timezone) {
                        form.appendChild(timezone);
                    }
                });
                form.submit();
            };
        });
    }
}).observe(document.body, { childList: true, subtree: true });
=======
    defineCustomElement(
        (props, ctx) => {
            const internalProps = reactive({ ...props });

            const createInput = (name, value) => {
                const input = document.createElement("input");
                input.type = "hidden";
                input.name = name;
                input.value = value;
                return input;
            };
            const dateTimeInputRef = ref(createInput(ctx.attrs.inputName, ctx.attrs.timestamp));
            const timezoneInputRef = ref(createInput(ctx.attrs.timezoneFieldName, ctx.attrs.timezone));
            const toDateTimeInputRef = ref(createInput(ctx.attrs.toInputName, ctx.attrs.toTimestamp));

            const emitValueChange = (detail) => {
                ctx.emit("change", detail);
            };

            const createInputDataHolders = () => {
                const shadowRoot = document.querySelector(`datetime-picker[input-name="${ctx.attrs.inputName}"]`);
                shadowRoot.appendChild(dateTimeInputRef.value);

                if (ctx.attrs.toInputName) {
                    shadowRoot.appendChild(toDateTimeInputRef.value);
                }

                if (ctx.attrs.enableTimezonePicker) {
                    shadowRoot.appendChild(timezoneInputRef.value);
                }
            };

            onMounted(() => {
                createInputDataHolders();
            });

            watch(
                () => props,
                (newProps) => {
                    Object.keys(newProps).forEach((key) => {
                        internalProps[key] = newProps[key];
                    });
                },
                { immediate: true, deep: true }
            );

            return () =>
                h(
                    App,
                    {
                        ...internalProps,
                        emitValueChange,
                        dateTimeInput: dateTimeInputRef.value,
                        timezoneInput: timezoneInputRef.value,
                        toDateTimeInput: toDateTimeInputRef.value,
                        rangePicker: Boolean(ctx.attrs.toInputName),
                    },
                    ctx.slots
                );
        },
        {
            styles: [styles],
        }
    )
);
>>>>>>> upstream/master
